package com.fiap.aoj.ms.relationShip.dao;

public class SistemaVsMicroservice {
	private int sistema_id;
	private int microservice_id;
	private Boolean status;
	
	public int getSistema_id() {
		return sistema_id;
	}
	public void setSistema_id(int sistema_id) {
		this.sistema_id = sistema_id;
	}
	public int getMicroservice_id() {
		return microservice_id;
	}
	public void setMicroservice_id(int microservice_id) {
		this.microservice_id = microservice_id;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
}
