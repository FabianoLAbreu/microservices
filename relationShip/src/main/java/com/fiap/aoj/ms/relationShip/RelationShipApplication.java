package com.fiap.aoj.ms.relationShip;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RelationShipApplication {

	public static void main(String[] args) {
		SpringApplication.run(RelationShipApplication.class, args);
	}
}
