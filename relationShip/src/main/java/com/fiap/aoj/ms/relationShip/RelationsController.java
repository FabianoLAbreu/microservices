package com.fiap.aoj.ms.relationShip;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/relations")
public class RelationsController {
	@RequestMapping(method = RequestMethod.GET, value = "/start", produces = "text/plain")
	public String start(){
		return "método inicial.";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public String createSistema(){
		return "método create.";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/delete")
	public String deleteSistema(){
		return "método delete.";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/update")
	public String updateSistema(){
		return "método update.";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/list")
	public String listSistema(){
		return "método list.";
	}
}
