package com.fiap.aoj.ms.relationShip.dao;

public class MicroserviceVsMicroservice {
	private int microservice_id;
	private int microservice_relation_id;
	private int sistema_id;
	
	public int getMicroservice_id() {
		return microservice_id;
	}
	public void setMicroservice_id(int microservice_id) {
		this.microservice_id = microservice_id;
	}
	public int getMicroservice_relation_id() {
		return microservice_relation_id;
	}
	public void setMicroservice_relation_id(int microservice_relation_id) {
		this.microservice_relation_id = microservice_relation_id;
	}
	public int getSistema_id() {
		return sistema_id;
	}
	public void setSistema_id(int sistema_id) {
		this.sistema_id = sistema_id;
	}
}
