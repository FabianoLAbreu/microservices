package com.fiap.aoj.ms.microservices.dao;

import java.util.Date;

public class Microservice {
	private int id;
	private String nome;
	private String end_point;
	private String nomecontainer;
	private Date data;
	private Boolean isativo;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEnd_point() {
		return end_point;
	}
	public void setEnd_point(String end_point) {
		this.end_point = end_point;
	}
	public String getNomecontainer() {
		return nomecontainer;
	}
	public void setNomecontainer(String nomecontainer) {
		this.nomecontainer = nomecontainer;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public Boolean getIsativo() {
		return isativo;
	}
	public void setIsativo(Boolean isativo) {
		this.isativo = isativo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
