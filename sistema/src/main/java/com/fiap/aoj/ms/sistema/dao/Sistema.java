package com.fiap.aoj.ms.sistema.dao;

public class Sistema {
	private int id;
	private String nome;
	private String host;
	private double versao;
	private int ms_id;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public double getVersao() {
		return versao;
	}
	public void setVersao(double versao) {
		this.versao = versao;
	}
	public int getMs_id() {
		return ms_id;
	}
	public void setMs_id(int ms_id) {
		this.ms_id = ms_id;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
