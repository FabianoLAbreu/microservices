package com.fiap.aoj.ms.basicMs;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class User {
	public User() {
	}
	
	public User(Long id, String name, int age) {
		this.id = id;
		this.name = name;
		this.age = age;
	}
	
	@Id
	private Long id;
	private String name;
	private int age;
	
	public String getNome() {
		return name;
	}
	public void setNome(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return "User{"+
				"id=" + id +
				", name='" + name + '\'' +
				", age=" + age +
				'}';
	}
}
